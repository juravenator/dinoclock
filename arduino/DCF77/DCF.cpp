#include "DCF.h"
// #include <time.h>
#include <Arduino.h>

template <typename T>
bool withinMargin(T val, T target, T margin) {
  return target - margin < val && val < target + margin;
}
#define WITHIN withinMargin<unsigned long>

// https://en.wikipedia.org/wiki/DCF77/
// DCF77 is a German longwave time signal and standard-frequency radio station.
// The DCF77 time signal is used for the dissemination of the German national legal time to the public.

// This code is tested on the following boards:
// - https://gemischtwaren-haendler.de/shopdateien/3942_1.pdf

// example usage:
// DCF dcf1;
// void setup() {
//   dcf1.setup(2, handlePin2);
// }
// void handlePin2() {
//   dcf1.handleInterrupt();
// }
void DCF::setup(int pin, void (*f)()) {
  _pin = pin;
  pinMode(_pin, INPUT);
  attachInterrupt(digitalPinToInterrupt(_pin), (*f), CHANGE);
  // Serial.println("setup done");
}

// we can only attach one interrupt to a pin
void DCF::handleInterrupt() {
  bool val = digitalRead(_pin);
  if (val) {
    handleRise();
  } else {
    handleFall();
  }
}

bool DCF::addToBuffer(bool val) {
  if (bufferPos == 59) {
    return false;
  }
  _buffer[bufferPos++] = val;
  // Serial.print(val, DEC);
  return true;
}

// DCF77 sends one logical bit of data each second, encoded by the length of a logical HIGH (1) pulse.
// A logical HIGH is denoted by the reduction of the amplitude of the carrier wave to 15% of normal amplitude.
// A logical HIGH for .1s denotes a binary 0
// A logical HIGH for .2s denotes a binary 1
// The last second of each minute is denoted by the absence of a HIGH signal
// On our input pin, a reduction in carrier amplitude is represented by a HIGH value.

// validation 1:
// a fluke 0 (denoted '+') during a valid 1 pulse
// s         s+        s
// 111000000010100000001110000000
// <---1s---> |
// noise      ^
// won't fix
// a fluke 0 in a pulse is unlikely, since the carrier amplitude is reduced to 15%
// (remember that carrier amplitude down = pin HIGH)
// if this occurs regularly, you have bigger problems

// validation 2:
// suppose we just booted up, and we have a noisy (denoted '+') signal
// s     +   s     +   s         s     +   s    +    s    +    s
// 1110001000111000100011100000001110001000111001000000000100001
// <---1s--->    | |   |         |     |   <--last 2s of min-->
// boot moment   ^ |   |         |     |   |
// first rise      ^   |         |     |   |                     (1) no previous rise to compare with (LR=0), LR=now SYNC=0
// LR !=~1s ago    <-->^         |     |   |                     (2) should restart (SYNC==0), LR=now SYNC=0
// last rise =~1s ago  <---1s--->^     |   |                     (3) we got in sync (LR!=0), LR=now, SYNC=1
// last rise !=~1s ago           <---->^   |                     (4) should reject, (SYNC=1), LR=LR
// last rise =~1s ago            <---1s--->^                     (5) still in sync (LR!=0), LR=now, SYNC=1
// last rise !=~1s ago                     <--->^                (4) should reject, (SYNC=1), LR=LR
// last rise !=~1s ago                              <---->^      (4) should reject, (SYNC=1), LR=LR
// last rise =~2s ago, and SYNC=1          <---1s---><---1s--->^ (3) still in sync (LR!=0 && SYNC=1), LR=now, SYNC=1
void DCF::handleRise() {
  unsigned long now = millis();
  unsigned long riseDiff = now - lastRise;
  ticksSinceLastSync++;
  Serial.print(".");

  bool validSecondPulse = WITHIN(riseDiff, DCF_PULSE_TIME, DCF_PULSE_TIME_MARGIN);
  bool validMinutePulse = WITHIN(riseDiff, DCF_END_OF_MINUTE_PULSE_TIME, DCF_PULSE_TIME_MARGIN);

  // validation 2
  if (lastRise == 0) { // (1)
    lastRise=now;
    return;
  } else if (validSecondPulse || validMinutePulse) { // (3)
    lastRise = now;
    lastRiseInSync = true;

    if (WITHIN(currentPeriodHIGH, DCF_PULSE_WIDTH_0, DCF_PULSE_WIDTH_MARGIN)) {
      bitsSinceLastSync++;
      if (!addToBuffer(0)) {
        bufferPos = 0;
      }
    } else if (WITHIN(currentPeriodHIGH, DCF_PULSE_WIDTH_1, DCF_PULSE_WIDTH_MARGIN)) {
      bitsSinceLastSync++;
      if (!addToBuffer(1)) {
        bufferPos = 0;
      }
    } else {
      // much too long pulse (the short ones are filtered in DCF77Fall)
      // could be lots of things, including being falsely in sync
      lastRiseInSync = false;
    }
    currentPeriodHIGH = 0;

    if (validMinutePulse) { // (5)
      minuteMarksSinceLastSync++;
      Serial.println("m");
      byte p = bufferPos;
      bufferPos = 0;
      if (p >= 59) {
        _latestPayload = parseBuffer(_buffer);
        _p_latestPayload = &_latestPayload;
      }
    }
  } else if (riseDiff > DCF_END_OF_MINUTE_PULSE_TIME) { // SERIOUSLY bad signal
    lastRise = now;
    currentPeriodHIGH = 0;
    bufferPos = 0;
    lastRiseInSync = false;
  } else if (lastRiseInSync) { // (4)
  } else { // (2)
    lastRise = now;
    currentPeriodHIGH = 0;
    bufferPos = 0;
  }
}

void DCF::handleFall() {
  unsigned long now = millis();
  unsigned long pulseDuration = now - lastRise;

  // validation 3:
  // a fluke 1
  // 11100000001110001000111000000011100000001110000000
  // <---1s--->
  // fluke 1---------^
  // this can be detected at fall time
  if (pulseDuration < DCF_PULSE_WIDTH_TOO_SHORT) {
    return;
  }
  currentPeriodHIGH += pulseDuration;
}

// The time is represented in binary-coded decimal.
// It represents civil time, including summer time adjustments.
// The time transmitted is the time of the following minute
// e.g. during December 31 23:59, the transmitted time encodes January 1 00:00

// DCF bits
// index | meaning
// 0     | start of minute, always 0
// 1-14  | civil warnings (only a draft proposal)
//       | and weather info (proprietary)
// 15    | abnormal transmitter operation
// 16    | next hour summer time change warning
// 17    | CEST in effect
// 18    | CET in effect
// 19    | next hour leap second warning
// 20    | always 1
// 21-27 | BCD minutes (0-59)
// 28    | even parity over bits 21-28
// 29-34 | BCD hours (0-23)
// 35    | even parity over bits 29-35
// 36-41 | BCD day of month (1-31)
// 42-44 | BCD day of week (1-7)
// 45-49 | BCD month (1-12)
// 50-57 | BCD year (0-99)
// 58    | even parity over bits 36–58
// 59    | N/A

// note that a leap second will result in an extra 0
// bit (1s) being transferred before the end-of-minute mark
// (missing amplitude drop)
// this is why our buffer is 60 slots wide, not 59

DCFPayload DCF::parseBuffer(bool buffer[60]) {
  DCFPayload payload = {};
  payload.abnormal_transmitter_operation = buffer[15];
  payload.summer_time_imminent = buffer[16];
  payload.cest = buffer[17];
  payload.cet = buffer[17];
  payload.leap_second_imminent = buffer[18];
  tm time = { 0 };
  time.tm_sec = 0; // 0-60

  if (!checkParityFail(buffer, 21, 28)) {
    time.tm_min = BCDToBinary(buffer, 21, 27); // 0-59
  } else {
    payload.parity1_error = 1;
  }
  if (!checkParityFail(buffer, 29, 35)) {
    time.tm_hour = BCDToBinary(buffer, 29, 34); // 0-23
  } else {
    payload.parity2_error = 1;
  }
  if (!checkParityFail(buffer, 36, 58)) {
    time.tm_mday = BCDToBinary(buffer, 36, 41); // 1-31
    // time.tm_wday = 0; // 0-6
    time.tm_mon = BCDToBinary(buffer, 45, 49) - 1; // 0-11
    time.tm_year = 2000 - 1900 + BCDToBinary(buffer, 50, 57);
  } else {
    payload.parity3_error = 1;
  }

  payload.time = time;
  // Serial.println("DCF: received time ");
  // Serial.print(time.tm_year, DEC); Serial.print("-");
  // Serial.print(time.tm_mon, DEC); Serial.print("-");
  // Serial.print(time.tm_mday, DEC); Serial.print(" ");
  // Serial.print(time.tm_hour, DEC); Serial.print(":");
  // Serial.print(time.tm_min, DEC); Serial.print(":");
  // Serial.println("00");
  return payload;
}

// the used BCD encoding is, from first-received bit to last:
// 1 2 4 8 10 20 40 80
const byte DCF::BCDWeights[] = {1, 2, 4, 8, 10, 20, 40, 80};

byte DCF::BCDToBinary(bool *buffer, byte start, byte end) {
  byte result = 0;
  for (byte i = start; i <= end; i++) {
    if (buffer[i]) {
      result += BCDWeights[i - start];
    }
  }
  return result;
}

bool DCF::checkParityFail(bool *buffer, byte start, byte end) {
  bool parity = false;
  for (byte i = start; i <= end; i++) {
    parity ^= buffer[i];
  }
  return parity;
}

DCFPayload* DCF::getLatestPayload() {
  DCFPayload* p = _p_latestPayload;
  _p_latestPayload = NULL;
  return p;
}