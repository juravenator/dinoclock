#ifndef DCF_h
#define DCF_h

// #define byte uint8_t

#include <Arduino.h>

#include <time.h>

// The DCF77 signal uses amplitude-shift keying to transmit digitally coded 
// time information by reducing the amplitude of the carrier to 15% of 
// normal (−16½ dB) for 0.1 or 0.2 seconds at the beginning of each second. 
// A 0.1 second reduction (7750 cycles of the 77500 Hz carrier amplitude) 
// denotes a binary 0; a 0.2 second reduction denotes a binary 1. As a 
// special case, the last second of every minute is marked with no carrier 
// power reduction.
// On our input pin, a reduction in carrier amplitude is represented by a HIGH value.
#define DCF_PULSE_TIME 1000
#define DCF_PULSE_TIME_MARGIN 40
#define DCF_PULSE_WIDTH_0 100
#define DCF_PULSE_WIDTH_1 200
#define DCF_PULSE_WIDTH_MARGIN 40
// the last second of the minute-long sequence will have no carrier power reduction
// therefore, an start of new minute mark will be recognized by a double pulse time
#define DCF_END_OF_MINUTE_PULSE_TIME DCF_PULSE_TIME * 2

// if we observe a HIGH input period shorter than this, surely this was invalid input
#define DCF_PULSE_WIDTH_TOO_SHORT DCF_PULSE_WIDTH_0/2

struct DCFPayload {
  bool parity1_error;
  bool parity2_error;
  bool parity3_error;
  unsigned long receivedAtMillis;
  bool abnormal_transmitter_operation;
  bool summer_time_imminent;
  bool leap_second_imminent;
  bool cest;
  bool cet;
  tm   time; // http://www.cplusplus.com/reference/ctime/tm/
};

class DCF {
  public:
    void setup(int pin, void (*f)());
    DCFPayload* getLatestPayload();
    void handleInterrupt();
  private:
    int _pin;
    int ticksSinceLastSync;
    int bitsSinceLastSync;
    int minuteMarksSinceLastSync;
    // The DCF77 station signal carries an amplitude-modulated, pulse-width coded 1 bit/s data signal.
    // The transmitted data signal is repeated every minute.
    bool _buffer[60];
    byte bufferPos;
    // the last millis() time there was a (valid?) first HIGH of a pulse
    unsigned long lastRise = 0;
    // how long, in total, we've been observing 1s since lastRise
    unsigned long currentPeriodHIGH = 0;
    bool lastRiseInSync = false;
    DCFPayload _latestPayload;
    DCFPayload *_p_latestPayload;

    static const byte BCDWeights[];

    void handleRise();
    void handleFall();
    bool addToBuffer(bool val);

    static DCFPayload parseBuffer(bool buffer[60]);
    static byte BCDToBinary(bool *buffer, byte start, byte end);
    static bool checkParityFail(bool *buffer, byte start, byte end);
};

#include "DCF.cpp"
#endif