#include <SPI.h>
#include <SD.h>

Sd2Card card;
SdVolume volume;
File f;

const int SS_PORT = 10;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial);
  
  Serial.println("starting");
  if (!SD.begin(SS_PORT)) {
    Serial.println("SD Card not inserted or not setup correctly");
    while (1);
  }
  Serial.println("SD card OK");

  File folder = SD.open("/");
  listFiles(folder);

}

void listFiles(File root) {
  while(true) {
    File f = root.openNextFile();
    if (!f) {
      break;
    }
    Serial.print("- ");
    Serial.print(f.name());
    if (f.isDirectory()) {
      Serial.println("/");
    } else {
      Serial.print("\t");
      Serial.println(f.size(), DEC);
      while (f.available()) {
        Serial.write(f.read());
      }
    }
    f.close();
  }
}

void loop() {

  

}
