// Example use of lfnOpenNext and open by index.
// You can use test files located in
// SdFat/examples/LongFileName/testFiles.
#include<SPI.h>
#include "SdFat.h"
#include "FreeStack.h"

// SD card chip select pin.
const uint8_t SD_CS_PIN = SS;

SdFat sd;
SdFile file;
SdFile dirFile;

void setup() {
  Serial.begin(9600);
  while (!Serial) {
    SysCall::yield();
  }
  Serial.println("starting");

  // Initialize at the highest speed supported by the board that is
  // not over 50 MHz. Try a lower speed if SPI errors occur.
  if (!sd.begin(SD_CS_PIN, SD_SCK_MHZ(50))) {
    sd.initErrorHalt();
  }

  // List files in root directory.
  if (!dirFile.open("/", O_RDONLY)) {
    sd.errorHalt("open root failed");
  }
  while (file.openNext(&dirFile, O_RDONLY)) {

    // Skip directories and hidden files.
    if (!file.isSubDir() && !file.isHidden()) {
      Serial.write("- ");
      file.printName(&Serial);
      Serial.println();
      while (file.available()) {
        Serial.write(file.read());
      }
    }
    file.close();
  }
}

void loop() {
  delay(100);
}
