#include "DCF77/DCF.h"
#include "clock/clock.h"
#include "lunar/lunar.h"

#define DISTANCE_TO_BRUSSELS_M 342886.67
#define SPEED_OF_LIGHT_M_S 299792458
#define SPEED_OF_LIGHT_DELAY_MS DISTANCE_TO_BRUSSELS_M * 1000 / SPEED_OF_LIGHT_M_S

DCF dcf1;
Clock clock1;
Lunar lunar;

void setup() {
  Serial.begin(115200);
  clock1 = Clock();
  while (!Serial);
  Serial.println("booting");
  dcf1.setup(2, handlePin2);
  Serial.println("speed of light compensation is " + String(SPEED_OF_LIGHT_DELAY_MS, DEC) + "ms");
  clock1.setCallback(&printTime);
  lunar.doStuff();
  // pinMode(6, OUTPUT);
  pinMode(3, INPUT);
  attachInterrupt(digitalPinToInterrupt(3), boop, RISING);
  // analogWrite(6, 100);
}

void boop() {
  Serial.println("boop");
}

void handlePin2() {
  // Serial.println("pin2 interrupt");
  dcf1.handleInterrupt();
}

unsigned long millies = millis();

void loop() {
  DCFPayload* dcfPayload = dcf1.getLatestPayload();
  if (dcfPayload != NULL && !dcfPayload->parity1_error && !dcfPayload->parity2_error && !dcfPayload->parity3_error) {
    clock1.setTime(&dcfPayload->time, SPEED_OF_LIGHT_DELAY_MS);
    Serial.println("DCF: received time ");
    printTime(&dcfPayload->time);
  }

  clock1.update();

  digitalWrite(LED_BUILTIN, digitalRead(3));

  // if (Serial.available() != 0) {
  //   // read the incoming byte:
  //   incomingByte = Serial.read();

  //   // say what you got:
  //   Serial.print("I received: ");
  //   Serial.println(incomingByte, DEC);
  // }

  // int servoPWM = millis() % 20;
  // if (servoPWM < 10) {
  //   digitalWrite(6, HIGH);
  // } else {
  //   digitalWrite(6, LOW);
  // }
}

void printTime(tm* time) {
  Serial.print(time->tm_year + 1900, DEC); Serial.print("-");
  Serial.print(time->tm_mon + 1, DEC); Serial.print("-");
  Serial.print(time->tm_mday, DEC); Serial.print(" ");
  Serial.print(time->tm_hour, DEC); Serial.print(":");
  Serial.print(time->tm_min, DEC); Serial.print(":");
  Serial.println(time->tm_sec, DEC);
}