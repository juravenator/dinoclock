#include "clock.h"
#include <Arduino.h>

Clock::Clock(void) {
  Serial.println("setting up clock");
  time(&_timeCounter);
  _millisLastUpdate = 0;
}

void Clock::setCallback(void (*cb)(tm* time)) {
  _updateTimeCb = cb;
}

void Clock::notify() {
  _updateTimeCb(localtime(&_timeCounter));
}

void Clock::update() {
  if (_timeCounter == 0) {
    return; // no use
  }
  unsigned long millisSinceLastCall = millis() - _millisLastUpdate;
  if (millisSinceLastCall >= 1000) {
    _millisLastUpdate += 1000;
    _timeCounter += 1;
    notify();
  }
}

void Clock::setTime(tm *tm, unsigned long offset) {
  _millisLastUpdate = millis() - offset;

  // time_t rawtime;
  // time(&rawtime);
  // struct tm* timeinfo = localtime (&_timeCounter);
  struct tm timeinfo = {};
  timeinfo.tm_year = tm->tm_year;
  timeinfo.tm_mon = tm->tm_mon;
  timeinfo.tm_mday = tm->tm_mday;
  timeinfo.tm_hour = tm->tm_hour;
  timeinfo.tm_min = tm->tm_min;
  _timeCounter = mktime(&timeinfo);
  
  // Serial.println("clock settime received");
  // Serial.print(timeinfo.tm_year, DEC); Serial.print("-");
  // Serial.print(timeinfo.tm_mon, DEC); Serial.print("-");
  // Serial.print(timeinfo.tm_mday, DEC); Serial.print("- ");
  // Serial.print(timeinfo.tm_hour, DEC); Serial.print(":");
  // Serial.print(timeinfo.tm_min, DEC); Serial.print(":");
  // _timeCounter = rawtime;
  // time->tm_yday = 1900;
  // _timeCounter = mktime(time);
  // _time = *time;
  notify();
}

// tm* Clock::getCurrentTime() {
//   return &_time;
// }