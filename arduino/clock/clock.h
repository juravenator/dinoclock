#ifndef CLOCK_h
#define CLOCK_h

#include <time.h>

class Clock {
  public:
    Clock();
    void setCallback(void (*cb)(tm* time));
    void update();
    void setTime(tm *time, unsigned long error);
    // tm* getCurrentTime();
  private:
    void (*_updateTimeCb)(tm* time);
    void notify();
    unsigned long _millisLastUpdate;
    time_t _timeCounter; // http://www.cplusplus.com/reference/ctime/tm/
    // tm _time;
};

#include "clock.cpp"
#endif