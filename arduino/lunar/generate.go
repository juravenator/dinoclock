//go:generate go run generator.go
// +build ignore

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type Entry struct {
	Year       int
	YearDelta  int
	Month      int
	MonthDelta int
	Day        int
}
type templateData struct {
	Timestamp  time.Time
	Startyear  int
	Startmonth int
	Entries    []Entry
}

func download(url string, filename string) {
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		res, err := http.Get(url)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		defer res.Body.Close()
		if res.StatusCode != 200 {
			fmt.Println(res.StatusCode, res.Status)
			os.Exit(1)
		}
		bytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		err = ioutil.WriteFile(filename, bytes, os.ModePerm)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}

func getData(startyear int) templateData {
	download("http://astropixels.com/ephemeris/phasescat/phases2001.html", "phases2001.html")
	download("http://astropixels.com/ephemeris/phasescat/phases2101.html", "phases2101.html")
	f1, err := os.Open("phases2001.html")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	f2, err := os.Open("phases2101.html")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	doc1, err := goquery.NewDocumentFromReader(f1)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	doc2, err := goquery.NewDocumentFromReader(f2)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	currentyear := 0
	entries := make([]Entry, 0)
	startMonth := 0

	preFunc := func(i int, pre *goquery.Selection) {
		// example text
		// Year      New Moon       First Quarter       Full Moon       Last Quarter <br/>
		// 2001                     Jan  2  22:31     Jan  9  20:24 t   Jan 16  12:35
		// 			 Jan 24  13:07     Feb  1  14:02     Feb  8  07:12     Feb 15  03:24
		// 			 Feb 23  08:21     Mar  3  02:03     Mar  9  17:23     Mar 16  20:45
		// 			 Mar 25  01:21     Apr  1  10:49     Apr  8  03:22     Apr 15  15:31
		// 			 Apr 23  15:26     Apr 30  17:08     May  7  13:53     May 15  10:11
		// 			 May 23  02:46     May 29  22:09     Jun  6  01:39     Jun 14  03:28
		// 			 Jun 21  11:58 T   Jun 28  03:20     Jul  5  15:04 p   Jul 13  18:45
		// 			 Jul 20  19:44     Jul 27  10:08     Aug  4  05:56     Aug 12  07:53
		// 			 Aug 19  02:55     Aug 25  19:55     Sep  2  21:43     Sep 10  19:00
		// 			 Sep 17  10:27     Sep 24  09:31     Oct  2  13:49     Oct 10  04:20
		// 			 Oct 16  19:23     Oct 24  02:58     Nov  1  05:41     Nov  8  12:21
		// 			 Nov 15  06:40     Nov 22  23:21     Nov 30  20:49     Dec  7  19:52
		// 			 Dec 14  20:48 A   Dec 22  20:56     Dec 30  10:41 n
		if strings.Trim(pre.Text(), " \n") != "" { // filter out empty pre tags
			for _, line := range strings.Split(pre.Text(), "\n") {

				if line == "" || strings.HasPrefix(line, " Year") { // empty/header line
					continue
				}

				entry := parseLine(line)

				if entry.Year != 0 {
					currentyear = entry.Year
					// if no startyear input was given, set it to first encountered year
					if startyear == 0 {
						startyear = currentyear
					}
				} else {
					entry.Year = currentyear
				}

				// if empty column for new moon, skip
				// possible on new year (=new line) with new moon in previous year
				if entry.Month == 0 {
					continue
				}
				if len(entries) == 0 {
					if currentyear < startyear {
						continue
					}
					// if no previous entries, set starting month
					startMonth = entry.Month
				} else {
					lastEntry := entries[len(entries)-1]
					// if year not specified, it did not change
					if entry.Year == 0 {
						entry.Year = lastEntry.Year
						entry.YearDelta = 0
						// if not same as previous year, there was an uptick
					} else if lastEntry.Year != entry.Year {
						entry.YearDelta = 1
					}

					entry.MonthDelta = entry.Month - lastEntry.Month
					// correct dec -> jan overflow
					if lastEntry.Month > entry.Month {
						entry.MonthDelta += 12
					}
				}
				entries = append(entries, entry)
			}
		}
	}
	doc1.Find("pre").Each(preFunc)
	doc2.Find("pre").Each(preFunc)
	return templateData{
		Timestamp:  time.Now(),
		Startyear:  startyear,
		Startmonth: startMonth,
		Entries:    entries,
	}
}

func parseLine(line string) Entry {
	// example lines
	//        Sep 19  01:08     Sep 26  18:19     Oct  3  08:54     Oct 10  13:10
	//        Oct 18  17:00     Oct 26  02:35     Nov  1  19:09     Nov  9  08:32
	//        Nov 17  08:21     Nov 24  10:03     Dec  1  08:11 n   Dec  9  05:48
	//        Dec 16  22:39 A   Dec 23  17:43     Dec 31  00:00
	// Year      New Moon       First Quarter       Full Moon       Last Quarter
	// 2086                                                         Jan  8  03:06
	//        Jan 15  11:25     Jan 22  02:42     Jan 29  17:49     Feb  6  22:31
	//        Feb 13  22:28     Feb 20  13:49     Feb 28  12:22     Mar  8  14:31
	//        Mar 15  08:05     Mar 22  03:17     Mar 30  06:18     Apr  7  02:23
	//        Apr 13  16:54     Apr 20  18:41     Apr 28  22:36     May  6  10:27
	//        May 13  01:42     May 20  11:20     May 28  12:36 p   Jun  4  15:52
	entry := Entry{}
	if strings.HasPrefix(line, " 2") {
		y, err := strconv.ParseInt(line[1:5], 10, 32)
		if err != nil {
			fmt.Println("invalid year", line[1:5])
			os.Exit(1)
		}
		entry.Year = int(y)
	}
	if line[8:9] != " " {
		entry.Month = getMonthNr(line[8:11])
		if d, err := strconv.ParseInt(strings.Trim(line[12:14], " "), 10, 32); err != nil {
			fmt.Println("invalid date", line[8:14])
			os.Exit(1)
		} else {
			entry.Day = int(d)
		}
	}
	return entry
}

func getMonthNr(month string) int {
	month = strings.ToLower(month[0:3])
	if month == "jan" {
		return 1
	} else if month == "feb" {
		return 2
	} else if month == "mar" {
		return 3
	} else if month == "apr" {
		return 4
	} else if month == "may" {
		return 5
	} else if month == "jun" {
		return 6
	} else if month == "jul" {
		return 7
	} else if month == "aug" {
		return 8
	} else if month == "sep" {
		return 9
	} else if month == "oct" {
		return 10
	} else if month == "nov" {
		return 11
	} else if month == "dec" {
		return 12
	}
	fmt.Println("invalid month", month)
	os.Exit(1)
	return 0
}

func main() {
	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	data := getData(2020)

	fileName := "lunar_progmem.cpp"
	fullPath := path.Join(cwd, fileName)
	fmt.Println("generating", fullPath)

	f, err := os.Create(fullPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = progmemTemplate.Execute(f, data)
	if err != nil {
		panic(err)
	}
}

var progmemTemplate = template.Must(template.New("").Parse(`// Code generated by go generate; DO NOT EDIT.
// This file was generated by robots at {{ .Timestamp }}
// sourced from http://astropixels.com/ephemeris/phasescat/phasescat.html

#ifndef LUNAR_PROGMEM_h
#define LUNAR_PROGMEM_h

#include <Arduino.h>

#define LUNAR_START_YEAR {{ .Startyear }}
#define LUNAR_START_MONTH {{ .Startmonth }}

const uint8_t lunar_dates[] PROGMEM = {
{{- range $entry := .Entries }}
    ({{ $entry.YearDelta }} << 7) | ({{ $entry.MonthDelta }} << 5) | {{ $entry.Day }}, // {{ $entry.Year }}-{{ $entry.Month }}-{{ $entry.Day }}
{{- end }}
};

#endif
`))
