// http://astropixels.com/ephemeris/phasescat/phases2001.html

#include "lunar.h"
#include "lunar_progmem.cpp"

void Lunar::doStuff() {
  uint16_t year = LUNAR_START_YEAR;
  uint8_t month = LUNAR_START_MONTH;
  Serial.println("there are " + String(sizeof(lunar_dates), DEC) + " lunar dates in the truth table");

  uint16_t l = sizeof(lunar_dates)/sizeof(lunar_dates[0]);
  for (uint16_t i = 0; i < l; i++) {
    uint8_t displayInt = pgm_read_byte_near(lunar_dates + i);
    bool yearUptick = (displayInt >> 7) & 0x01;
    bool monthUptick = (displayInt >> 5) && 0x01;
    uint8_t day = displayInt & 0x1F;

    if (yearUptick) {
      year += 1;
      month = 0;
    }
    if (monthUptick) {
      month += 1;
    }
    // Serial.println(String(year, DEC) + " " + String(month, DEC) + "-" + String(day, DEC));
  }
  Serial.println("start at " + String(LUNAR_START_YEAR, DEC) + "-" + String(LUNAR_START_MONTH, DEC) + ", end at "  + String(year, DEC) + "-" + String(month, DEC));
}