#ifndef LUNAR_h
#define LUNAR_h

#include <Arduino.h>

struct lunar_entry {
  bool monthUptick;
  // byte month; // 1-12
  byte day; // 1-31
};

class Lunar {
  public:
    void doStuff();
    // Clock();
    // void setCallback(void (*cb)(tm* time));
    // void tickTockTickTock();
    // void setTime(tm *time, unsigned long error);
    // // tm* getCurrentTime();
  private:
    // unsigned int index;
    // void (*_updateTimeCb)(tm* time);
    // void notify();
    // unsigned long _millisLastUpdate;
    // time_t _timeCounter; // http://www.cplusplus.com/reference/ctime/tm/
    // // tm _time;
};

#include "lunar_progmem.cpp"
#include "lunar.cpp"
#endif