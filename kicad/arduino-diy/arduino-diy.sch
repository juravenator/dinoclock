EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328P-AU U4
U 1 1 5F8318CD
P 6750 4750
F 0 "U4" H 6750 3161 50  0000 C CNN
F 1 "ATmega328P-AU" H 6750 3070 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 6750 4750 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 6750 4750 50  0001 C CNN
F 4 "C14877" H 6750 4750 50  0001 C CNN "LCSC"
	1    6750 4750
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATmega:ATmega328P-MU U6
U 1 1 5F832C4E
P 9500 4750
F 0 "U6" H 9500 3161 50  0000 C CNN
F 1 "ATmega328P-MU" H 9500 3070 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-32-1EP_5x5mm_P0.5mm_EP3.1x3.1mm" H 9500 4750 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 9500 4750 50  0001 C CNN
F 4 "C783592" H 9500 4750 50  0001 C CNN "LCSC"
	1    9500 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5F85742F
P 10550 5100
F 0 "R5" V 10343 5100 50  0000 C CNN
F 1 "10K" V 10434 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 10480 5100 50  0001 C CNN
F 3 "~" H 10550 5100 50  0001 C CNN
	1    10550 5100
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR023
U 1 1 5F8599C5
P 10100 3100
F 0 "#PWR023" H 10100 2950 50  0001 C CNN
F 1 "+5V" H 10115 3273 50  0000 C CNN
F 2 "" H 10100 3100 50  0001 C CNN
F 3 "" H 10100 3100 50  0001 C CNN
	1    10100 3100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR028
U 1 1 5F869110
P 10800 2500
F 0 "#PWR028" H 10800 2250 50  0001 C CNN
F 1 "GND" H 10805 2327 50  0000 C CNN
F 2 "" H 10800 2500 50  0001 C CNN
F 3 "" H 10800 2500 50  0001 C CNN
	1    10800 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 2450 10250 2500
Wire Wire Line
	10250 2100 10250 2150
$Comp
L Device:C C11
U 1 1 5F86E806
P 10550 2500
F 0 "C11" V 10390 2500 50  0000 C CNN
F 1 "22pF" V 10299 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10588 2350 50  0001 C CNN
F 3 "~" H 10550 2500 50  0001 C CNN
	1    10550 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10250 2100 10400 2100
Wire Wire Line
	10250 2500 10400 2500
$Comp
L Device:C C10
U 1 1 5F864BD7
P 10550 2100
F 0 "C10" V 10298 2100 50  0000 C CNN
F 1 "22pF" V 10389 2100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10588 1950 50  0001 C CNN
F 3 "~" H 10550 2100 50  0001 C CNN
	1    10550 2100
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal_GND24 Y3
U 1 1 5F8380E1
P 10250 2300
F 0 "Y3" V 10296 2056 50  0000 R CNN
F 1 "16MHz" V 10205 2056 50  0000 R CNN
F 2 "Crystal:Crystal_SMD_3225-4Pin_3.2x2.5mm" H 10250 2300 50  0001 C CNN
F 3 "~" H 10250 2300 50  0001 C CNN
F 4 "C13738" H 10250 2300 50  0001 C CNN "LCSC"
	1    10250 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9600 3250 9500 3250
Text Label 10100 4050 0    50   ~ 0
MU_D13_SCK
Text Label 10100 3950 0    50   ~ 0
MU_D12_MISO
Text Label 10100 3850 0    50   ~ 0
MU_D11_MOSI
Text Label 10100 3550 0    50   ~ 0
MU_D8
Text Label 10100 3650 0    50   ~ 0
MU_D9
Text Label 10100 3750 0    50   ~ 0
MU_D10
Text Label 10100 4450 0    50   ~ 0
MU_A0
Text Label 10100 4550 0    50   ~ 0
MU_A1
Text Label 10100 4650 0    50   ~ 0
MU_A2
Text Label 10100 4750 0    50   ~ 0
MU_A3
Text Label 10100 4850 0    50   ~ 0
MU_A4
Text Label 10100 4950 0    50   ~ 0
MU_A5
Text Label 8900 3750 2    50   ~ 0
MU_A6
Text Label 8900 3850 2    50   ~ 0
MU_A7
Text Label 10100 5250 0    50   ~ 0
MU_D0_RX
Text Label 10100 5350 0    50   ~ 0
MU_D1_TX
Text Label 10100 5450 0    50   ~ 0
MU_D2
Text Label 10100 5550 0    50   ~ 0
MU_D3
Text Label 10100 5650 0    50   ~ 0
MU_D4
Text Label 8700 4200 2    50   ~ 0
MU_D5
Text Label 10100 5850 0    50   ~ 0
MU_D6
Text Label 10100 5950 0    50   ~ 0
MU_D7
Wire Wire Line
	10450 2300 10800 2300
Wire Wire Line
	10800 2300 10800 2500
Wire Wire Line
	10700 2500 10800 2500
Connection ~ 10800 2500
Wire Wire Line
	10700 2100 10800 2100
Wire Wire Line
	10800 2100 10800 2300
Connection ~ 10800 2300
Text Label 13300 4550 2    50   ~ 0
MU_A6
Text Label 13300 4450 2    50   ~ 0
MU_A7
Text Label 10250 2100 2    50   ~ 0
MU_XTAL1
Text Label 10250 2500 2    50   ~ 0
MU_XTAL2
Text Label 10100 4150 0    50   ~ 0
MU_XTAL1
Text Label 10100 4250 0    50   ~ 0
MU_XTAL2
$Comp
L power:+5V #PWR016
U 1 1 5FB2CE81
P 7350 3100
F 0 "#PWR016" H 7350 2950 50  0001 C CNN
F 1 "+5V" H 7365 3273 50  0000 C CNN
F 2 "" H 7350 3100 50  0001 C CNN
F 3 "" H 7350 3100 50  0001 C CNN
	1    7350 3100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5FB2CE95
P 8050 2500
F 0 "#PWR018" H 8050 2250 50  0001 C CNN
F 1 "GND" H 8055 2327 50  0000 C CNN
F 2 "" H 8050 2500 50  0001 C CNN
F 3 "" H 8050 2500 50  0001 C CNN
	1    8050 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2450 7500 2500
Wire Wire Line
	7500 2100 7500 2150
$Comp
L Device:C C7
U 1 1 5FB2CEA1
P 7800 2500
F 0 "C7" V 7640 2500 50  0000 C CNN
F 1 "22pF" V 7549 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 2350 50  0001 C CNN
F 3 "~" H 7800 2500 50  0001 C CNN
	1    7800 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7500 2100 7650 2100
Wire Wire Line
	7500 2500 7650 2500
$Comp
L Device:C C6
U 1 1 5FB2CEAD
P 7800 2100
F 0 "C6" V 7548 2100 50  0000 C CNN
F 1 "22pF" V 7639 2100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7838 1950 50  0001 C CNN
F 3 "~" H 7800 2100 50  0001 C CNN
	1    7800 2100
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal_GND24 Y2
U 1 1 5FB2CEB8
P 7500 2300
F 0 "Y2" V 7546 2056 50  0000 R CNN
F 1 "16MHz" V 7455 2056 50  0000 R CNN
F 2 "Crystal:Crystal_SMD_3225-4Pin_3.2x2.5mm" H 7500 2300 50  0001 C CNN
F 3 "~" H 7500 2300 50  0001 C CNN
F 4 "C13738" H 7500 2300 50  0001 C CNN "LCSC"
	1    7500 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 3250 6750 3250
Text Label 7350 4050 0    50   ~ 0
AU_D13_SCK
Text Label 7350 3950 0    50   ~ 0
AU_D12_MISO
Text Label 7350 3850 0    50   ~ 0
AU_D11_MOSI
Text Label 7350 3550 0    50   ~ 0
AU_D8
Text Label 7350 3650 0    50   ~ 0
AU_D9
Text Label 7350 3750 0    50   ~ 0
AU_D10
Text Label 7350 4450 0    50   ~ 0
AU_A0
Text Label 7350 4550 0    50   ~ 0
AU_A1
Text Label 7350 4650 0    50   ~ 0
AU_A2
Text Label 7350 4850 0    50   ~ 0
AU_A4
Text Label 7350 4950 0    50   ~ 0
AU_A5
Text Label 6150 3750 2    50   ~ 0
AU_A6
Text Label 6150 3850 2    50   ~ 0
AU_A7
Text Label 7350 5250 0    50   ~ 0
AU_D0_RX
Text Label 7350 5350 0    50   ~ 0
AU_D1_TX
Text Label 7350 5450 0    50   ~ 0
AU_D2
Text Label 7350 5550 0    50   ~ 0
AU_D3
Text Label 7350 5650 0    50   ~ 0
AU_D4
Text Label 7350 5850 0    50   ~ 0
AU_D6
Text Label 7350 5950 0    50   ~ 0
AU_D7
Wire Wire Line
	7700 2300 8050 2300
Wire Wire Line
	8050 2300 8050 2500
Wire Wire Line
	7950 2500 8050 2500
Connection ~ 8050 2500
Wire Wire Line
	7950 2100 8050 2100
Wire Wire Line
	8050 2100 8050 2300
Connection ~ 8050 2300
$Comp
L Connector_Generic:Conn_01x05 J10
U 1 1 5FB2CF4E
P 5200 5300
F 0 "J10" H 5100 5500 50  0000 L CNN
F 1 "A" H 5280 5201 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 5200 5300 50  0001 C CNN
F 3 "~" H 5200 5300 50  0001 C CNN
	1    5200 5300
	1    0    0    -1  
$EndComp
Text Label 5000 5200 2    50   ~ 0
AU_A6
Text Label 5000 5300 2    50   ~ 0
AU_A7
Text Label 7500 2100 2    50   ~ 0
AU_XTAL1
Text Label 7500 2500 2    50   ~ 0
AU_XTAL2
Text Label 7350 4150 0    50   ~ 0
AU_XTAL1
Text Label 7350 4250 0    50   ~ 0
AU_XTAL2
$Comp
L Connector_Generic:Conn_01x03 J16
U 1 1 5FC742D4
P 10550 900
F 0 "J16" H 10630 942 50  0000 L CNN
F 1 "Conn_01x03" H 10630 851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 10550 900 50  0001 C CNN
F 3 "~" H 10550 900 50  0001 C CNN
	1    10550 900 
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR025
U 1 1 5FC88A11
P 10350 900
F 0 "#PWR025" H 10350 750 50  0001 C CNN
F 1 "+3V3" V 10365 1028 50  0000 L CNN
F 2 "" H 10350 900 50  0001 C CNN
F 3 "" H 10350 900 50  0001 C CNN
	1    10350 900 
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR024
U 1 1 5FCAC051
P 10350 800
F 0 "#PWR024" H 10350 650 50  0001 C CNN
F 1 "+5V" V 10365 928 50  0000 L CNN
F 2 "" H 10350 800 50  0001 C CNN
F 3 "" H 10350 800 50  0001 C CNN
	1    10350 800 
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5FCB81E2
P 10350 1000
F 0 "#PWR026" H 10350 750 50  0001 C CNN
F 1 "GND" V 10355 872 50  0000 R CNN
F 2 "" H 10350 1000 50  0001 C CNN
F 3 "" H 10350 1000 50  0001 C CNN
	1    10350 1000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5FD08969
P 6750 6250
F 0 "#PWR015" H 6750 6000 50  0001 C CNN
F 1 "GND" V 6755 6122 50  0000 R CNN
F 2 "" H 6750 6250 50  0001 C CNN
F 3 "" H 6750 6250 50  0001 C CNN
	1    6750 6250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5FD0A47D
P 9500 6250
F 0 "#PWR021" H 9500 6000 50  0001 C CNN
F 1 "GND" V 9505 6122 50  0000 R CNN
F 2 "" H 9500 6250 50  0001 C CNN
F 3 "" H 9500 6250 50  0001 C CNN
	1    9500 6250
	0    -1   -1   0   
$EndComp
$Comp
L RF_Module:ESP32-S2-WROVER U3
U 1 1 5F8CB6A3
P 2950 1900
F 0 "U3" H 2950 3281 50  0000 C CNN
F 1 "ESP32-S2-WROVER" H 2950 3190 50  0000 C CNN
F 2 "RF_Module:ESP32-S2-WROVER" H 3700 750 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-s2-wroom_esp32-s2-wroom-i_datasheet_en.pdf" H 2650 1100 50  0001 C CNN
	1    2950 1900
	1    0    0    -1  
$EndComp
$Comp
L RF_Module:ESP32-WROOM-32 U2
U 1 1 5F90AC10
P 2500 -2350
F 0 "U2" H 2500 -769 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 2500 -860 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 2500 -3850 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 2200 -2300 50  0001 C CNN
	1    2500 -2350
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR01
U 1 1 5F991D31
P 450 -3750
F 0 "#PWR01" H 450 -3900 50  0001 C CNN
F 1 "+3V3" V 465 -3622 50  0000 L CNN
F 2 "" H 450 -3750 50  0001 C CNN
F 3 "" H 450 -3750 50  0001 C CNN
	1    450  -3750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5F9994B7
P 1050 -3500
F 0 "R2" V 843 -3500 50  0000 C CNN
F 1 "10K" V 934 -3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 980 -3500 50  0001 C CNN
F 3 "~" H 1050 -3500 50  0001 C CNN
	1    1050 -3500
	-1   0    0    1   
$EndComp
$Comp
L Device:C C1
U 1 1 5F9BF4F8
P 1050 -3200
F 0 "C1" H 1165 -3154 50  0000 L CNN
F 1 "1uF" H 1165 -3245 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1088 -3350 50  0001 C CNN
F 3 "~" H 1050 -3200 50  0001 C CNN
	1    1050 -3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5F9E6002
P 2500 -950
F 0 "#PWR010" H 2500 -1200 50  0001 C CNN
F 1 "GND" V 2505 -1078 50  0000 R CNN
F 2 "" H 2500 -950 50  0001 C CNN
F 3 "" H 2500 -950 50  0001 C CNN
	1    2500 -950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 -3350 1450 -3350
Wire Wire Line
	1450 -3350 1450 -3550
Wire Wire Line
	1450 -3550 1900 -3550
Connection ~ 1050 -3350
$Comp
L power:GND #PWR04
U 1 1 5FA173A3
P 1050 -3050
F 0 "#PWR04" H 1050 -3300 50  0001 C CNN
F 1 "GND" V 1055 -3178 50  0000 R CNN
F 2 "" H 1050 -3050 50  0001 C CNN
F 3 "" H 1050 -3050 50  0001 C CNN
	1    1050 -3050
	1    0    0    -1  
$EndComp
Text Label 3100 -3550 0    50   ~ 0
IO0_BOOT
Text Label 3100 -3450 0    50   ~ 0
IO1_TX
Text Label 3100 -3350 0    50   ~ 0
IO2
Text Label 3100 -3250 0    50   ~ 0
IO3_RX
Text Label 3100 -3150 0    50   ~ 0
IO4
Text Label 3100 -3050 0    50   ~ 0
IO5
Text Label 3100 -2950 0    50   ~ 0
IO12
Text Label 3100 -2850 0    50   ~ 0
IO13
Text Label 3100 -2750 0    50   ~ 0
IO14
Text Label 3100 -2650 0    50   ~ 0
IO15
Text Label 3100 -2550 0    50   ~ 0
IO16
Text Label 3100 -2450 0    50   ~ 0
IO17
Text Label 3100 -2350 0    50   ~ 0
IO18
Text Label 3100 -2250 0    50   ~ 0
IO19
Text Label 3100 -2150 0    50   ~ 0
IO21
Text Label 3100 -2050 0    50   ~ 0
IO22
Text Label 3100 -1950 0    50   ~ 0
IO23
Text Label 3100 -1850 0    50   ~ 0
IO25
Text Label 3100 -1750 0    50   ~ 0
IO26
Text Label 3100 -1650 0    50   ~ 0
IO27
Text Label 3100 -1550 0    50   ~ 0
IO32
Text Label 3100 -1450 0    50   ~ 0
IO33
Text Label 3100 -1350 0    50   ~ 0
IO34
Text Label 3100 -1250 0    50   ~ 0
IO35
Text Label 1900 -2350 2    50   ~ 0
IO7_SD0
Text Label 1900 -2250 2    50   ~ 0
IO8_SD1
Text Label 1900 -2150 2    50   ~ 0
IO9_SD2
Text Label 1900 -2050 2    50   ~ 0
IO10_SD3
Text Label 1900 -1950 2    50   ~ 0
IO6_CLK
Text Label 1900 -1850 2    50   ~ 0
IO11_CMD
Text Label 1900 -3350 2    50   ~ 0
IO20_SVP
Text Label 1900 -3250 2    50   ~ 0
IO24_SVN
Text Label 4100 -750 2    50   ~ 0
IO19
Text Label 5450 -250 2    50   ~ 0
IO20_SVP
Text Label 4100 -650 2    50   ~ 0
IO21
Text Label 4100 -350 2    50   ~ 0
IO22
Text Label 4100 -250 2    50   ~ 0
IO23
Text Label 5450 -350 2    50   ~ 0
IO24_SVN
Text Label 5450 -850 2    50   ~ 0
IO25
Text Label 5450 -950 2    50   ~ 0
IO26
Text Label 5450 -1050 2    50   ~ 0
IO27
$Comp
L Connector_Generic:Conn_01x12 J11
U 1 1 5FD3406C
P 5650 -750
F 0 "J11" H 5700 167 50  0000 C CNN
F 1 "IO" H 5700 76  50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 5650 -750 50  0001 C CNN
F 3 "~" H 5650 -750 50  0001 C CNN
	1    5650 -750
	1    0    0    -1  
$EndComp
Text Label 4100 -850 2    50   ~ 0
IO18
Text Label 4100 -1050 2    50   ~ 0
IO17
Text Label 4100 -1350 2    50   ~ 0
IO0_BOOT
Text Label 4100 -450 2    50   ~ 0
IO1_TX
Text Label 4000 -3500 2    50   ~ 0
IO2
Text Label 4100 -550 2    50   ~ 0
IO3_RX
Text Label 4100 -1250 2    50   ~ 0
IO4
Text Label 4100 -950 2    50   ~ 0
IO5
Text Label 4000 -3100 2    50   ~ 0
IO6_CLK
Text Label 4000 -3200 2    50   ~ 0
IO7_SD0
Text Label 4000 -3300 2    50   ~ 0
IO8_SD1
Text Label 4000 -2800 2    50   ~ 0
IO9_SD2
Text Label 4000 -2900 2    50   ~ 0
IO10_SD3
Text Label 4000 -3000 2    50   ~ 0
IO11_CMD
Text Label 4000 -2700 2    50   ~ 0
IO13
Text Label 5450 -1250 2    50   ~ 0
IO12
Text Label 5450 -1150 2    50   ~ 0
IO14
Text Label 4100 -1150 2    50   ~ 0
IO16
Text Label 4000 -3400 2    50   ~ 0
IO15
Text Label 5450 -650 2    50   ~ 0
IO32
Text Label 5450 -750 2    50   ~ 0
IO33
Text Label 5450 -450 2    50   ~ 0
IO34
Text Label 5450 -550 2    50   ~ 0
IO35
Text Label 2350 1000 2    50   ~ 0
S2_IO0
$Comp
L power:GND #PWR012
U 1 1 5FDA69E6
P 2950 3000
F 0 "#PWR012" H 2950 2750 50  0001 C CNN
F 1 "GND" V 2955 2872 50  0000 R CNN
F 2 "" H 2950 3000 50  0001 C CNN
F 3 "" H 2950 3000 50  0001 C CNN
	1    2950 3000
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR03
U 1 1 5FDEA87C
P 500 700
F 0 "#PWR03" H 500 550 50  0001 C CNN
F 1 "+3V3" V 515 828 50  0000 L CNN
F 2 "" H 500 700 50  0001 C CNN
F 3 "" H 500 700 50  0001 C CNN
	1    500  700 
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5FDEA886
P 1100 950
F 0 "R3" V 893 950 50  0000 C CNN
F 1 "10K" V 984 950 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1030 950 50  0001 C CNN
F 3 "~" H 1100 950 50  0001 C CNN
	1    1100 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C C2
U 1 1 5FDEA890
P 1100 1250
F 0 "C2" H 1215 1296 50  0000 L CNN
F 1 "1uF" H 1215 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1138 1100 50  0001 C CNN
F 3 "~" H 1100 1250 50  0001 C CNN
	1    1100 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1100 1500 1100
Wire Wire Line
	1500 1100 1500 900 
Connection ~ 1100 1100
$Comp
L power:GND #PWR05
U 1 1 5FDEA8A2
P 1100 1400
F 0 "#PWR05" H 1100 1150 50  0001 C CNN
F 1 "GND" V 1105 1272 50  0000 R CNN
F 2 "" H 1100 1400 50  0001 C CNN
F 3 "" H 1100 1400 50  0001 C CNN
	1    1100 1400
	1    0    0    -1  
$EndComp
Text Label 2350 1100 2    50   ~ 0
S2_IO1
Text Label 2350 1200 2    50   ~ 0
S2_IO2
Text Label 2350 1300 2    50   ~ 0
S2_IO3
Text Label 2350 1400 2    50   ~ 0
S2_IO4
Text Label 2350 1500 2    50   ~ 0
S2_IO5
Text Label 2350 1600 2    50   ~ 0
S2_IO6
Text Label 2350 1700 2    50   ~ 0
S2_IO7
Text Label 2350 1800 2    50   ~ 0
S2_IO8
Text Label 2350 1900 2    50   ~ 0
S2_IO9
Text Label 2350 2000 2    50   ~ 0
S2_IO10
Text Label 2350 2100 2    50   ~ 0
S2_IO11
Text Label 2350 2200 2    50   ~ 0
S2_IO12
Text Label 2350 2300 2    50   ~ 0
S2_IO13
Text Label 2350 2400 2    50   ~ 0
S2_IO14
Text Label 2350 2500 2    50   ~ 0
S2_IO15
Text Label 2350 2600 2    50   ~ 0
S2_IO16
Text Label 2350 2700 2    50   ~ 0
S2_IO17
Text Label 2350 2800 2    50   ~ 0
S2_IO18
Text Label 3550 900  0    50   ~ 0
S2_IO46
Text Label 3550 1000 0    50   ~ 0
S2_IO45
Text Label 3550 1100 0    50   ~ 0
S2_IO44_RX
Text Label 3550 1200 0    50   ~ 0
S2_IO43_TX
Text Label 3550 1300 0    50   ~ 0
S2_IO42
Text Label 3550 1400 0    50   ~ 0
S2_IO41
Text Label 3550 1500 0    50   ~ 0
S2_IO40
Text Label 3550 1600 0    50   ~ 0
S2_IO39
Text Label 3550 1700 0    50   ~ 0
S2_IO38
Text Label 3550 1800 0    50   ~ 0
S2_IO37
Text Label 3550 1900 0    50   ~ 0
S2_IO36
Text Label 3550 2000 0    50   ~ 0
S2_IO35
Text Label 3550 2100 0    50   ~ 0
S2_IO34
Text Label 3550 2200 0    50   ~ 0
S2_IO33
Text Label 3550 2300 0    50   ~ 0
S2_IO26
Text Label 3550 2400 0    50   ~ 0
S2_IO21
Text Label 3550 2500 0    50   ~ 0
S2_IO20_D+
Text Label 3550 2600 0    50   ~ 0
S2_IO19_D-
Text Label 4550 950  2    50   ~ 0
S2_IO0
Text Label 4550 1050 2    50   ~ 0
S2_IO1
Text Label 4550 1150 2    50   ~ 0
S2_IO2
Text Label 4550 1250 2    50   ~ 0
S2_IO3
Text Label 4550 1350 2    50   ~ 0
S2_IO4
Text Label 4550 1450 2    50   ~ 0
S2_IO5
Text Label 4550 1550 2    50   ~ 0
S2_IO6
Text Label 4550 1650 2    50   ~ 0
S2_IO7
Text Label 4550 1750 2    50   ~ 0
S2_IO8
Text Label 4550 1850 2    50   ~ 0
S2_IO9
Text Label 4550 1950 2    50   ~ 0
S2_IO10
Text Label 4550 2050 2    50   ~ 0
S2_IO11
Text Label 4550 2150 2    50   ~ 0
S2_IO12
Text Label 4550 2250 2    50   ~ 0
S2_IO13
Text Label 4550 2350 2    50   ~ 0
S2_IO14
Text Label 4550 2450 2    50   ~ 0
S2_IO15
Text Label 4550 2550 2    50   ~ 0
S2_IO16
Text Label 4550 2650 2    50   ~ 0
S2_IO17
Text Label 4550 2750 2    50   ~ 0
S2_IO18
Wire Wire Line
	1500 900  2350 900 
Text Label 2350 900  2    50   ~ 0
S2_EN
Text Label 5950 4250 2    50   ~ 0
AU_D7
Text Label 5950 4350 2    50   ~ 0
AU_D8
Text Label 5950 4550 2    50   ~ 0
AU_D10
Text Label 5950 4450 2    50   ~ 0
AU_D9
Text Label 5950 4650 2    50   ~ 0
AU_D11_MOSI
Text Label 5950 4750 2    50   ~ 0
AU_D12_MISO
Text Label 5000 5100 2    50   ~ 0
AU_D13_SCK
Text Label 8700 4400 2    50   ~ 0
MU_D7
Text Label 8700 4500 2    50   ~ 0
MU_D8
Text Label 8700 4600 2    50   ~ 0
MU_D9
Text Label 8700 4700 2    50   ~ 0
MU_D10
Text Label 8700 4800 2    50   ~ 0
MU_D11_MOSI
Text Label 8700 4900 2    50   ~ 0
MU_D12_MISO
Text Label 13300 4650 2    50   ~ 0
MU_D13_SCK
$Comp
L Connector_Generic:Conn_01x12 J7
U 1 1 6044BCFC
P 4300 -850
F 0 "J7" H 4350 67  50  0000 C CNN
F 1 "IO" H 4350 -24 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 4300 -850 50  0001 C CNN
F 3 "~" H 4300 -850 50  0001 C CNN
	1    4300 -850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x09 J6
U 1 1 60541EE1
P 4200 -3100
F 0 "J6" H 4200 -2650 50  0000 C CNN
F 1 "IO" H 4200 -2750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 4200 -3100 50  0001 C CNN
F 3 "~" H 4200 -3100 50  0001 C CNN
	1    4200 -3100
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Dual JP4
U 1 1 5F95DBB2
P 5950 3250
F 0 "JP4" H 5950 3489 50  0000 C CNN
F 1 "AREF Select" H 5950 3398 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5950 3250 50  0001 C CNN
F 3 "~" H 5950 3250 50  0001 C CNN
	1    5950 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR014
U 1 1 5F95E58C
P 6200 3250
F 0 "#PWR014" H 6200 3100 50  0001 C CNN
F 1 "+3V3" V 6215 3378 50  0000 L CNN
F 2 "" H 6200 3250 50  0001 C CNN
F 3 "" H 6200 3250 50  0001 C CNN
	1    6200 3250
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR013
U 1 1 5F95EDD2
P 5700 3250
F 0 "#PWR013" H 5700 3100 50  0001 C CNN
F 1 "+5V" H 5715 3423 50  0000 C CNN
F 2 "" H 5700 3250 50  0001 C CNN
F 3 "" H 5700 3250 50  0001 C CNN
	1    5700 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6150 3550 5950 3550
Wire Wire Line
	5950 3550 5950 3350
$Comp
L MCU_Microchip_ATmega:ATmega32U4-MU U1
U 1 1 5FA09292
P 1900 5250
F 0 "U1" H 1900 3361 50  0000 C CNN
F 1 "ATmega32U4-MU" H 1900 3270 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-44-1EP_7x7mm_P0.5mm_EP5.2x5.2mm" H 1900 5250 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf" H 1900 5250 50  0001 C CNN
	1    1900 5250
	1    0    0    -1  
$EndComp
Text Label 5950 3550 2    50   ~ 0
AREF_SELECT
Text Label 8900 3550 2    50   ~ 0
AREF_SELECT
Wire Wire Line
	6750 3100 6750 3250
Connection ~ 6750 3250
$Comp
L Device:Jumper JP6
U 1 1 5FB354CC
P 9800 3100
F 0 "JP6" H 9800 3364 50  0000 C CNN
F 1 "ENABLE" H 9800 3273 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9800 3100 50  0001 C CNN
F 3 "~" H 9800 3100 50  0001 C CNN
	1    9800 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 3250 9500 3100
Connection ~ 9500 3250
Wire Wire Line
	7350 5050 7650 5050
Wire Wire Line
	10100 5050 10400 5050
$Comp
L Connector_Generic:Conn_01x08 J14
U 1 1 5FBC0D3A
P 6150 4350
F 0 "J14" H 6200 4867 50  0000 C CNN
F 1 "D" H 6150 3850 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 6150 4350 50  0001 C CNN
F 3 "~" H 6150 4350 50  0001 C CNN
	1    6150 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 5050 10400 5100
$Comp
L Connector_Generic:Conn_01x08 J15
U 1 1 5FC32DED
P 8900 4500
F 0 "J15" H 8900 5000 50  0000 C CNN
F 1 "D" H 8900 5100 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 8900 4500 50  0001 C CNN
F 3 "~" H 8900 4500 50  0001 C CNN
	1    8900 4500
	1    0    0    -1  
$EndComp
Text Label 1750 -3550 0    50   ~ 0
EN
Text Label 5450 -150 2    50   ~ 0
EN
$Comp
L power:GND #PWR09
U 1 1 5FD42852
P 1900 7050
F 0 "#PWR09" H 1900 6800 50  0001 C CNN
F 1 "GND" V 1905 6922 50  0000 R CNN
F 2 "" H 1900 7050 50  0001 C CNN
F 3 "" H 1900 7050 50  0001 C CNN
	1    1900 7050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1800 7050 1900 7050
Connection ~ 1900 7050
$Comp
L power:GND #PWR08
U 1 1 5FD55BF5
P 1600 3100
F 0 "#PWR08" H 1600 2850 50  0001 C CNN
F 1 "GND" H 1605 2927 50  0000 C CNN
F 2 "" H 1600 3100 50  0001 C CNN
F 3 "" H 1600 3100 50  0001 C CNN
	1    1600 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 3050 1050 3100
Wire Wire Line
	1050 2700 1050 2750
$Comp
L Device:C C5
U 1 1 5FD55BFD
P 1350 3100
F 0 "C5" V 1190 3100 50  0000 C CNN
F 1 "22pF" V 1099 3100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1388 2950 50  0001 C CNN
F 3 "~" H 1350 3100 50  0001 C CNN
	1    1350 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1050 2700 1200 2700
Wire Wire Line
	1050 3100 1200 3100
$Comp
L Device:C C4
U 1 1 5FD55C05
P 1350 2700
F 0 "C4" V 1098 2700 50  0000 C CNN
F 1 "22pF" V 1189 2700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1388 2550 50  0001 C CNN
F 3 "~" H 1350 2700 50  0001 C CNN
	1    1350 2700
	0    1    1    0   
$EndComp
$Comp
L Device:Crystal_GND24 Y1
U 1 1 5FD55C0C
P 1050 2900
F 0 "Y1" V 1096 2656 50  0000 R CNN
F 1 "16MHz" V 1005 2656 50  0000 R CNN
F 2 "Crystal:Crystal_SMD_3225-4Pin_3.2x2.5mm" H 1050 2900 50  0001 C CNN
F 3 "~" H 1050 2900 50  0001 C CNN
F 4 "C13738" H 1050 2900 50  0001 C CNN "LCSC"
	1    1050 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1250 2900 1600 2900
Wire Wire Line
	1600 2900 1600 3100
Wire Wire Line
	1500 3100 1600 3100
Connection ~ 1600 3100
Wire Wire Line
	1500 2700 1600 2700
Wire Wire Line
	1600 2700 1600 2900
Connection ~ 1600 2900
Text Label 1050 2700 2    50   ~ 0
U4_XTAL1
Text Label 1050 3100 2    50   ~ 0
U4_XTAL2
Text Label 1300 3950 2    50   ~ 0
U4_XTAL1
Text Label 1300 4150 2    50   ~ 0
U4_XTAL2
Text Label 1300 4350 2    50   ~ 0
AREF_SELECT
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5FD67E8A
P 2950 5250
F 0 "J2" H 3050 5250 50  0000 C CNN
F 1 "D" H 3050 5150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 2950 5250 50  0001 C CNN
F 3 "~" H 2950 5250 50  0001 C CNN
	1    2950 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J5
U 1 1 5FD6BC77
P 3050 6350
F 0 "J5" H 3150 6350 50  0000 C CNN
F 1 "F" H 3150 6250 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 3050 6350 50  0001 C CNN
F 3 "~" H 3050 6350 50  0001 C CNN
	1    3050 6350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5FD7EA55
P 1100 4850
F 0 "J1" H 1200 4850 50  0000 C CNN
F 1 "USB" H 1200 4750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1100 4850 50  0001 C CNN
F 3 "~" H 1100 4850 50  0001 C CNN
	1    1100 4850
	-1   0    0    1   
$EndComp
$Comp
L Device:Jumper JP3
U 1 1 5FD81385
P 2300 3400
F 0 "JP3" H 2300 3664 50  0000 C CNN
F 1 "ENABLE" H 2300 3573 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 3400 50  0001 C CNN
F 3 "~" H 2300 3400 50  0001 C CNN
	1    2300 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR011
U 1 1 5FD81C7F
P 2600 3400
F 0 "#PWR011" H 2600 3250 50  0001 C CNN
F 1 "+5V" H 2615 3573 50  0000 C CNN
F 2 "" H 2600 3400 50  0001 C CNN
F 3 "" H 2600 3400 50  0001 C CNN
	1    2600 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 3400 2000 3450
Wire Wire Line
	2000 3400 1900 3400
Wire Wire Line
	1900 3400 1900 3450
Connection ~ 2000 3400
Wire Wire Line
	1900 3400 1800 3400
Wire Wire Line
	1800 3400 1800 3450
Connection ~ 1900 3400
$Comp
L Device:C C3
U 1 1 5FDC4CF0
P 1100 5250
F 0 "C3" H 1215 5296 50  0000 L CNN
F 1 "1uF" H 1215 5205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1138 5100 50  0001 C CNN
F 3 "~" H 1100 5250 50  0001 C CNN
	1    1100 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5FDC9B44
P 1100 5400
F 0 "#PWR06" H 1100 5150 50  0001 C CNN
F 1 "GND" V 1105 5272 50  0000 R CNN
F 2 "" H 1100 5400 50  0001 C CNN
F 3 "" H 1100 5400 50  0001 C CNN
	1    1100 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5050 1100 5050
Wire Wire Line
	1100 5050 1100 5100
$Comp
L Device:R R4
U 1 1 5FB2CE6D
P 7800 5050
F 0 "R4" V 7593 5050 50  0000 C CNN
F 1 "10K" V 7684 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7730 5050 50  0001 C CNN
F 3 "~" H 7800 5050 50  0001 C CNN
	1    7800 5050
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5FDD6E83
P 1000 3650
F 0 "R1" V 793 3650 50  0000 C CNN
F 1 "10K" V 884 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 930 3650 50  0001 C CNN
F 3 "~" H 1000 3650 50  0001 C CNN
	1    1000 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	1300 3750 1200 3750
Wire Wire Line
	1150 3650 1200 3650
Wire Wire Line
	1200 3650 1200 3750
Text Label 10100 5750 0    50   ~ 0
MU_D5
Text Label 8700 4300 2    50   ~ 0
MU_D6
Text Label 7350 5750 0    50   ~ 0
AU_D5
Text Label 5950 4050 2    50   ~ 0
AU_D5
Text Label 5950 4150 2    50   ~ 0
AU_D6
$Comp
L Connector_Generic:Conn_01x05 J4
U 1 1 5FEA0158
P 3050 4450
F 0 "J4" H 3150 4500 50  0000 C CNN
F 1 "B" H 3150 4400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 3050 4450 50  0001 C CNN
F 3 "~" H 3050 4450 50  0001 C CNN
	1    3050 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3750 2850 3600
Wire Wire Line
	2850 3700 2500 3850
Wire Wire Line
	2500 3950 2850 3800
Wire Wire Line
	2850 3900 2500 4050
Wire Wire Line
	2500 4450 2700 4450
Wire Wire Line
	2700 4450 2700 4150
Wire Wire Line
	2700 4150 2850 4000
Wire Wire Line
	2500 4350 2550 4350
Wire Wire Line
	2550 4350 2550 4400
Wire Wire Line
	2550 4400 2750 4400
Wire Wire Line
	1200 3650 1200 3450
Wire Wire Line
	1750 3450 1750 3250
Wire Wire Line
	1750 3250 2100 3250
Wire Wire Line
	2100 3250 2100 3500
Wire Wire Line
	2100 3500 2600 3500
Wire Wire Line
	2600 3500 2600 4100
Wire Wire Line
	2600 4100 2850 4100
Wire Wire Line
	1200 3450 1750 3450
Connection ~ 1200 3650
Wire Wire Line
	2750 4950 2500 4950
Wire Wire Line
	2500 5050 2750 5050
Wire Wire Line
	2750 5150 2500 5150
Wire Wire Line
	2500 5250 2750 5250
Wire Wire Line
	2750 5350 2600 5350
Wire Wire Line
	2600 5350 2600 5450
Wire Wire Line
	2600 5450 2500 5450
Wire Wire Line
	2500 5350 2550 5350
Wire Wire Line
	2550 5350 2550 5400
Wire Wire Line
	2550 5400 2650 5400
Wire Wire Line
	2650 5400 2650 5450
Wire Wire Line
	2650 5450 2750 5450
Wire Wire Line
	2500 5550 2750 5550
Wire Wire Line
	2500 5650 2750 5650
Text Label 5850 5250 2    50   ~ 0
AU_A4
Text Label 7350 4750 0    50   ~ 0
AU_A3
Text Label 5850 4950 2    50   ~ 0
AU_A3
Text Label 5850 5050 2    50   ~ 0
AU_A2
Text Label 5000 5500 2    50   ~ 0
AU_A1
Text Label 5000 5400 2    50   ~ 0
AU_A0
Text Label 5850 5150 2    50   ~ 0
AU_A5
Wire Wire Line
	7300 2300 7700 2300
Connection ~ 7700 2300
Wire Wire Line
	10050 2300 10450 2300
Connection ~ 10450 2300
Wire Wire Line
	850  2900 1250 2900
Connection ~ 1250 2900
$Comp
L Device:Jumper JP2
U 1 1 5F9699FC
P 800 700
F 0 "JP2" H 800 964 50  0000 C CNN
F 1 "ENABLE" H 800 873 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 800 700 50  0001 C CNN
F 3 "~" H 800 700 50  0001 C CNN
	1    800  700 
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP5
U 1 1 5FB23233
P 7050 3100
F 0 "JP5" H 7050 3364 50  0000 C CNN
F 1 "ENABLE" H 7050 3273 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7050 3100 50  0001 C CNN
F 3 "~" H 7050 3100 50  0001 C CNN
	1    7050 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP1
U 1 1 5F97DD8C
P 750 -3750
F 0 "JP1" H 750 -3486 50  0000 C CNN
F 1 "ENABLE" H 750 -3577 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 750 -3750 50  0001 C CNN
F 3 "~" H 750 -3750 50  0001 C CNN
	1    750  -3750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J9
U 1 1 5F96F1AE
P 4750 2750
F 0 "J9" H 4800 3867 50  0001 C CNN
F 1 "IO" H 4800 3776 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 4750 2750 50  0001 C CNN
F 3 "~" H 4750 2750 50  0001 C CNN
	1    4750 2750
	1    0    0    -1  
$EndComp
Text Label 4550 2850 2    50   ~ 0
S2_IO19_D-
Text Label 5550 2650 2    50   ~ 0
S2_EN
Text Label 5550 2750 2    50   ~ 0
S2_IO46
Text Label 5550 2450 2    50   ~ 0
S2_IO45
Text Label 5550 2550 2    50   ~ 0
S2_IO44_RX
Text Label 5550 2250 2    50   ~ 0
S2_IO43_TX
Text Label 5550 2350 2    50   ~ 0
S2_IO42
Text Label 5550 2150 2    50   ~ 0
S2_IO40
Text Label 5550 2050 2    50   ~ 0
S2_IO41
Text Label 5550 1850 2    50   ~ 0
S2_IO39
Text Label 5550 1950 2    50   ~ 0
S2_IO38
Text Label 5550 1650 2    50   ~ 0
S2_IO37
Text Label 5550 1750 2    50   ~ 0
S2_IO36
Text Label 5550 1450 2    50   ~ 0
S2_IO35
Text Label 5550 1550 2    50   ~ 0
S2_IO34
Text Label 4550 3250 2    50   ~ 0
S2_IO33
Text Label 4550 3150 2    50   ~ 0
S2_IO26
Text Label 4550 3050 2    50   ~ 0
S2_IO21
Text Label 4550 2950 2    50   ~ 0
S2_IO20_D+
$Comp
L Connector_Generic:Conn_01x14 J8
U 1 1 5FD7B9AC
P 4750 1550
F 0 "J8" H 4800 2667 50  0001 C CNN
F 1 "IO" H 4800 2576 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x07_P2.54mm_Vertical" H 4750 1550 50  0001 C CNN
F 3 "~" H 4750 1550 50  0001 C CNN
	1    4750 1550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J12
U 1 1 60404E33
P 5750 2050
F 0 "J12" H 5800 3167 50  0001 C CNN
F 1 "IO" H 5800 3076 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x07_P2.54mm_Vertical" H 5750 2050 50  0001 C CNN
F 3 "~" H 5750 2050 50  0001 C CNN
	1    5750 2050
	1    0    0    -1  
$EndComp
Text Label 13300 4350 2    50   ~ 0
MU_A0
Text Label 13300 4250 2    50   ~ 0
MU_A1
Text Label 12250 4250 2    50   ~ 0
MU_D0_RX
Text Label 12250 4350 2    50   ~ 0
MU_D1_TX
Text Label 12250 4450 2    50   ~ 0
MU_D2
Text Label 12250 3850 2    50   ~ 0
MU_A2
Text Label 12250 3750 2    50   ~ 0
MU_A3
Text Label 12250 4050 2    50   ~ 0
MU_A4
Text Label 12250 3950 2    50   ~ 0
MU_A5
Text Label 10100 5050 0    50   ~ 0
MU_RST
Text Label 12250 4150 2    50   ~ 0
MU_RST
$Comp
L Connector_Generic:Conn_01x05 J18
U 1 1 5FA5203A
P 13500 4450
F 0 "J18" H 13580 4396 50  0000 L CNN
F 1 "A" H 13580 4351 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 13500 4450 50  0001 C CNN
F 3 "~" H 13500 4450 50  0001 C CNN
	1    13500 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x10 J17
U 1 1 5FA401C0
P 12450 4150
F 0 "J17" H 12530 4096 50  0000 L CNN
F 1 "A" H 12530 4051 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 12450 4150 50  0001 C CNN
F 3 "~" H 12450 4150 50  0001 C CNN
	1    12450 4150
	1    0    0    -1  
$EndComp
Text Label 12250 4550 2    50   ~ 0
MU_D3
Text Label 12250 4650 2    50   ~ 0
MU_D4
$Comp
L Connector_Generic:Conn_01x10 J13
U 1 1 5F965032
P 6050 5350
F 0 "J13" H 6150 5550 50  0000 C CNN
F 1 "D" H 6150 5450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 6050 5350 50  0001 C CNN
F 3 "~" H 6050 5350 50  0001 C CNN
	1    6050 5350
	1    0    0    -1  
$EndComp
Text Label 7350 5050 0    50   ~ 0
AU_RST
Text Label 5850 5350 2    50   ~ 0
AU_RST
Text Label 5850 5450 2    50   ~ 0
AU_D0_RX
Text Label 5850 5550 2    50   ~ 0
AU_D1_TX
Text Label 5850 5650 2    50   ~ 0
AU_D2
Text Label 5850 5750 2    50   ~ 0
AU_D3
Text Label 5850 5850 2    50   ~ 0
AU_D4
Wire Wire Line
	2500 6150 2850 6150
Wire Wire Line
	2850 6050 2500 6050
Wire Wire Line
	2500 6050 2500 5950
Wire Wire Line
	2500 5850 2650 5850
Wire Wire Line
	2500 6650 2500 6750
Wire Wire Line
	2500 6750 2850 6750
Wire Wire Line
	2650 5850 2650 6650
Wire Wire Line
	2650 6650 2850 6650
Wire Wire Line
	2850 6250 2750 6250
Wire Wire Line
	2750 6250 2750 6350
Wire Wire Line
	2750 6350 2500 6350
Wire Wire Line
	2500 6250 2700 6250
Wire Wire Line
	2700 6250 2700 6300
Wire Wire Line
	2700 6300 2800 6300
Wire Wire Line
	2800 6300 2800 6350
Wire Wire Line
	2800 6350 2850 6350
Wire Wire Line
	2850 6550 2850 6500
Wire Wire Line
	2850 6500 2750 6500
Wire Wire Line
	2750 6500 2750 6450
Wire Wire Line
	2750 6450 2500 6450
Wire Wire Line
	2500 6550 2800 6550
Wire Wire Line
	2800 6550 2800 6450
Wire Wire Line
	2800 6450 2850 6450
Wire Wire Line
	2850 4750 2850 4650
Wire Wire Line
	2500 4750 2850 4750
Wire Wire Line
	2750 4550 2850 4550
Wire Wire Line
	2750 4400 2750 4550
Wire Wire Line
	2500 4500 2850 4500
Wire Wire Line
	2850 4500 2850 4450
Wire Wire Line
	2500 4500 2500 4650
Wire Wire Line
	2850 4350 2600 4350
Wire Wire Line
	2600 4350 2600 4150
Wire Wire Line
	2600 4150 2500 4150
Wire Wire Line
	2500 4250 2850 4250
$Comp
L Connector_Generic:Conn_01x06 J3
U 1 1 5FD66E2D
P 3050 3800
F 0 "J3" H 3150 3850 50  0000 C CNN
F 1 "B" H 3150 3750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 3050 3800 50  0001 C CNN
F 3 "~" H 3050 3800 50  0001 C CNN
	1    3050 3800
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AMS1117-3.3 U5
U 1 1 5FAA44DB
P 9300 800
F 0 "U5" H 9300 1042 50  0000 C CNN
F 1 "AMS1117-3.3" H 9300 951 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 9300 1000 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 9400 550 50  0001 C CNN
	1    9300 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5FABD13F
P 9000 950
F 0 "C8" H 8750 1000 50  0000 L CNN
F 1 "22uF" H 8700 900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9038 800 50  0001 C CNN
F 3 "~" H 9000 950 50  0001 C CNN
	1    9000 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5FAC97D9
P 9600 950
F 0 "C9" H 9715 996 50  0000 L CNN
F 1 "22uF" H 9715 905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9638 800 50  0001 C CNN
F 3 "~" H 9600 950 50  0001 C CNN
	1    9600 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR019
U 1 1 5FAD1B7E
P 9000 800
F 0 "#PWR019" H 9000 650 50  0001 C CNN
F 1 "+5V" V 9015 928 50  0000 L CNN
F 2 "" H 9000 800 50  0001 C CNN
F 3 "" H 9000 800 50  0001 C CNN
	1    9000 800 
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5FAD8A8C
P 9300 1100
F 0 "#PWR020" H 9300 850 50  0001 C CNN
F 1 "GND" H 9305 927 50  0000 C CNN
F 2 "" H 9300 1100 50  0001 C CNN
F 3 "" H 9300 1100 50  0001 C CNN
	1    9300 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR022
U 1 1 5FAD91D4
P 9600 800
F 0 "#PWR022" H 9600 650 50  0001 C CNN
F 1 "+3V3" V 9615 928 50  0000 L CNN
F 2 "" H 9600 800 50  0001 C CNN
F 3 "" H 9600 800 50  0001 C CNN
	1    9600 800 
	0    1    1    0   
$EndComp
Connection ~ 9600 800 
Connection ~ 9000 800 
Wire Wire Line
	9000 1100 9300 1100
Connection ~ 9300 1100
Wire Wire Line
	9300 1100 9600 1100
Wire Wire Line
	1050 -3750 2500 -3750
Wire Wire Line
	1050 -3650 1050 -3750
Connection ~ 1050 -3750
Wire Wire Line
	1100 700  2950 700 
Wire Wire Line
	1100 800  1100 700 
Connection ~ 1100 700 
Wire Wire Line
	1800 3400 850  3400
Wire Wire Line
	850  3400 850  3650
Connection ~ 1800 3400
Wire Wire Line
	6850 3250 7950 3250
Wire Wire Line
	7950 3250 7950 5050
Connection ~ 6850 3250
Wire Wire Line
	10700 5100 10700 3250
Wire Wire Line
	10700 3250 9600 3250
Connection ~ 9600 3250
Wire Wire Line
	850  3650 850  4550
Wire Wire Line
	850  4550 1300 4550
Connection ~ 850  3650
$EndSCHEMATC
