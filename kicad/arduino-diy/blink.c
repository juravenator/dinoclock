void setup() {
  Serial.begin(115200);
  pinMode(9, OUTPUT);
  Serial.println("setup");
}

void loop() {
  Serial.println("ON");
  digitalWrite(9, HIGH);
  delay(1000);
  Serial.println("OFF");
  digitalWrite(9, LOW);
  delay(1000);
}