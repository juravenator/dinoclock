# Programming the DIY board

# ESP32-WROOM-32E (U2)

**Important:** This board is **strictly** 3.3V on all pins (absolute max 3.6V). It is not 5V tolerant on any pins. Using 5V on any pins bricks your chip.

### Pinout

![pinout](esp32-pinout.jpg)

The number on the board reflect the `GPIOxx` labels on the pinout.

### Programming

To program this chip, you'll need a USB to TTL converter like this one:  
https://www.benl.ebay.be/itm/USB-zu-TTL-Converter-CP2102-UART-serieller-Konverter-6-Pin/162885014549

Note: theoretically, one could also use an arduino uno by shorting RESET to GROUND, and using the RX/TX pins from the onboard TTL chip. You will however need a voltage divider to convert the signals to 3.3V (3.6 absolute max).  
This is how I bricked one of my chips, just get the ebay thing.

![programming](esp32-programming.jpg)

| board | description |
|-|-|
| 0 (BOOT MODE) | connect to a switch that connects to ground |
| 1 (TX) | connect to RXD on usb to TTL board |
| 3 (RX) | connect to TXD on usb to TTL board |
| RST | connect to 3.3V with a 10K resistor, and a button that shorts to ground |

To enter programming mode, keep the BOOT MODE button pressed during the entire time Arduino IDE is programming the board, press RESET once to initiate programming.

You can program the ESP32 with arduino code using the esp32 plugin like described here:  
https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/  
In short, set `https://dl.espressif.com/dl/package_esp32_index.json` in board manager urls in preferences, and then install the esp32 plugin in board manager.

Use board `ESP32 Dev module` when programming the board.

The serial monitor will work normally like with an arduino.

### Serial communication
Serial communication can be done using the USB to TTL chip you used to program the chip.
The serial monitor will work normally like with an arduino. i.e. You can open the serial monitor.


# ATMega32u4 (U1)

### Pinout
You can consult the arduino leonardo pinout for reference:
![pinout](leonardo-pinout.jpg)

### Fuse settings
```bash
avrdude -c stk500v1 -p atmega32u4 -P /dev/ttyACM0 -v -b 19200 -U lfuse:w:0xFF:m -U hfuse:w:0x97:m -U efuse:w:0xC9:m
```
The default fuses for arduino are `L0xFF H0xD8 E0xCB`.
The difference is the minimal boot sector size, disabling EEPROM erase when programming, and brown-out detection trigger at 3.5V rather than 2.6V.

Fuses can be calculated here:  
https://www.engbedded.com/fusecalc

Do not that when using your custom fuse settings, extended fuses cover undefined bits, you cannot change them and don't actually do anything. Your avrdude command may give a verification error, check if the value it reads back out matches your desired config.

### Programming

Programming is done through another arduino, programmed using the 'Arduino as ISP' sketch one can find in the example scetches.

Select Arduino Leonardo in the board selection menu, and program using `sketch > upload using programmer`.

#### Wiring for programming

| Uno | board |
|-|-|
| 13 | B1 |
| 12 | B3 |
| 11 | B2 |
| 10 | RST |

Do not forget to connect the enable pins (jumper in the picture), and to connect 5V and GND.

![wiring](atmega32u4-programming.jpg)
![wiring](atmega32u4-programming-2.jpg)

### Serial communication
USB D+ and D- wires are built into this chip, you can connect a USB cable straight to the board.
The serial monitor will work normally like with an arduino. i.e. You can open the serial monitor.

![serial](atmega32u4-serial.jpg)

# ATMega328P-AU (U4)

### Pinout
You can consult the arduino uno pinout for reference:
![pinout](uno-pinout.jpg)

### Programming

Programming is done through another arduino, programmed using the 'Arduino as ISP' sketch one can find in the example scetches.

Select Arduino Leonardo in the board selection menu, and program using `sketch > upload using programmer`.

### Wiring for programming

| Uno | board |
|-|-|
| 13 | D13 (SCK) |
| 12 | D12 (MISO) |
| 11 | D11 (MOSI) |
| 10 | RST |

Do not forget to connect the enable pins (jumper in the picture), and to connect 5V and GND.

![wiring](atmega328p-au-programming.jpg)

### Fuse settings
```bash
avrdude -c stk500v1 -p atmega328p -P /dev/ttyACM0 -v -b 19200 -U lfuse:w:0xFF:m -U hfuse:w:0xDE:m -U efuse:w:0xFD:m
```
The default fuse settings for arduino are `L0xFF H0xDA E0xFD`.
The difference is the minimal boot sector size.

### Serial communication
You can use the USB to TTL chip from earlier, and connect the RX/TX pins to TX/RX on the board.
The serial monitor will work normally like with an arduino. i.e. You can open the serial monitor.

![serial](atmega328p-serial.jpg)

# ATMega328P-MU (U6)

Everything is identical to U4.

### Wiring for programming

![wiring](atmega328p-mu-programming.jpg)