# Steps to send this design to JLCPCB

1. Set version and commit hash in silkscreen

1. Generate gerber file
- https://support.jlcpcb.com/article/102-kicad-515---generating-gerber-and-drill-files
- `cd gerber && zip -r ../gerber.zip * && cd -`

1. Generate BOM & Centroid file  
- https://support.jlcpcb.com/article/84-how-to-generate-the-bom-and-centroid-file-from-kicad
- hearder for footprint file: `Designator,Val,Package,Mid X,Mid Y,Rotation,Layer`
- The BOM file is called `arduino-diy.csv`
- The footprint position file is called `arduino-diy-top-pos.csv`