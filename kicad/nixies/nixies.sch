EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L nixies-us:IN-14 N1
U 1 1 5F81EF75
P 1900 2750
F 0 "N1" H 1950 3579 45  0000 C CNN
F 1 "IN-14" H 1950 3495 45  0000 C CNN
F 2 "nixies-us:nixies-us-IN-14" H 1930 2900 20  0001 C CNN
F 3 "" H 1900 2750 50  0001 C CNN
	1    1900 2750
	1    0    0    -1  
$EndComp
$Comp
L nixies-us:IN-14 N2
U 1 1 5F81F886
P 3100 2750
F 0 "N2" H 3150 3579 45  0000 C CNN
F 1 "IN-14" H 3150 3495 45  0000 C CNN
F 2 "nixies-us:nixies-us-IN-14" H 3130 2900 20  0001 C CNN
F 3 "" H 3100 2750 50  0001 C CNN
	1    3100 2750
	1    0    0    -1  
$EndComp
$Comp
L nixies-us:IN-8 N3
U 1 1 5F81FE11
P 4850 2850
F 0 "N3" H 4800 3779 45  0000 C CNN
F 1 "IN-8" H 4800 3695 45  0000 C CNN
F 2 "nixies-us:nixies-us-IN-8" H 4880 3000 20  0001 C CNN
F 3 "" H 4850 2850 50  0001 C CNN
	1    4850 2850
	1    0    0    -1  
$EndComp
$Comp
L nixies-us:IN-8 N4
U 1 1 5F820549
P 6050 2850
F 0 "N4" H 6000 3779 45  0000 C CNN
F 1 "IN-8" H 6000 3695 45  0000 C CNN
F 2 "nixies-us:nixies-us-IN-8" H 6080 3000 20  0001 C CNN
F 3 "" H 6050 2850 50  0001 C CNN
	1    6050 2850
	1    0    0    -1  
$EndComp
$Comp
L nixies-us:IN-16 N5
U 1 1 5F820DAC
P 7400 2850
F 0 "N5" H 7450 3779 45  0000 C CNN
F 1 "IN-16" H 7450 3695 45  0000 C CNN
F 2 "nixies-us:nixies-us-IN-16" H 7430 3000 20  0001 C CNN
F 3 "" H 7400 2850 50  0001 C CNN
	1    7400 2850
	1    0    0    -1  
$EndComp
$Comp
L nixies-us:IN-16 N6
U 1 1 5F8213E0
P 8600 2850
F 0 "N6" H 8650 3779 45  0000 C CNN
F 1 "IN-16" H 8650 3695 45  0000 C CNN
F 2 "nixies-us:nixies-us-IN-16" H 8630 3000 20  0001 C CNN
F 3 "" H 8600 2850 50  0001 C CNN
	1    8600 2850
	1    0    0    -1  
$EndComp
$Comp
L nixies-misc:74141DIP16 M1
U 1 1 5F827BD4
P 5500 4950
F 0 "M1" H 5500 4950 45  0001 C CNN
F 1 "K[m]155ид1" H 5450 5620 45  0000 C CNN
F 2 "nixies-misc:nixiemisc-DIP16" H 5530 5100 20  0001 C CNN
F 3 "" H 5500 4950 50  0001 C CNN
	1    5500 4950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
