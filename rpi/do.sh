#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

if [[ -z "${1:-}" ]]; then
  >&2 echo "specify a command (start|stop)"
  exit 1
elif [[ "$1" == "start" ]]; then
  echo "starting setup network"
  cp stuff/dhcpcd.conf stuff/dnsmasq.conf /etc
  cp stuff/hostapd.conf /etc/hostapd
  systemctl restart dhcpcd
  systemctl enable --now hostapd dnsmasq
elif [[ "$1" == "stop" ]]; then
  echo "stopping setup network"
  rm /etc/dnsmasq.conf /etc/hostapd/hostapd.conf
  cp stuff/dhcpcd.conf.orig /etc/dhcpcd.conf
  systemctl disable --now hostapd dnsmasq
  systemctl restart dhcpcd
else
  >&2 echo "unknown command '$1'"
  exit 1
fi

